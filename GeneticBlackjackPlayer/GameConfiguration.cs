﻿using System;
using System.Collections.Generic;
using System.Linq;
    using System.Text;

namespace BlackNeuron
{
    static class Configuration
    {
        //number of decks to be shuffled together in the shoe
        public static  int Shoe_NumberOfDecks = 2;

        public static int Simulate_Rounds = 10000; //how many rounds of play should be done on each candidate before calculating the advantage?
        public static int Neural_HiddenNeurons = 20; //number of hidden neurons
        public static int Genetic_Population = 30; // population each each generation
        public static int Genetic_Elites = 3; //Always keep top 10.

        public static double Genetic_MutationFactor = .2; //weight can change +/- 1 whenever mutated
        public static double Genetic_MutationRate = 0.005; //2% chance of mutation on each weight

    }
}
