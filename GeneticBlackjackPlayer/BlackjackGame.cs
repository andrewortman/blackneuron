﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlackNeuron
{
    class BlackjackGame
    {
        private DealerShoe dealer_shoe;
        private Hand dealer_hand;
        private Hand player_hand;


        public BlackjackGame()
        {
            dealer_shoe = new DealerShoe(Configuration.Shoe_NumberOfDecks);
            dealer_hand = new Hand();
            player_hand = new Hand();
        }


        public void Reset()
        {
            dealer_hand.ResetHand();
            player_hand.ResetHand();
            dealer_shoe.ResetDeck();
        }

        public void NewRound()
        {
            dealer_hand.ResetHand();
            player_hand.ResetHand();
            
            if (!dealer_shoe.ShoeSuitableForNewGame()) dealer_shoe.ResetDeck();

            player_hand.AddCard(dealer_shoe.NextCard());
            player_hand.AddCard(dealer_shoe.NextCard());
            dealer_hand.AddCard(dealer_shoe.NextCard());
            dealer_hand.AddCard(dealer_shoe.NextCard());
        }


        public void PlayerHit()
        {
            player_hand.AddCard(dealer_shoe.NextCard());
        }

        public void DealerPlay()
        {
            //This method basically starts the dealer's game.
            for(;;)
            {
                if (dealer_hand.IsBust()) break;
                if (dealer_hand.GetScore() >= 17) break;
                dealer_hand.AddCard(dealer_shoe.NextCard());
            }
        }


        //This is called after the dealer plays.
        //Returns the percentage of the bet the player recieves back
        //-1.0 = lost (-100%)
        //0.0 = push (0%)
        //1.0 = standard win (100%)
        //1.5 = blackjack win (150%)

        public double GetResults()
        {
            if (player_hand.IsBust()) return -1.0; //player bust loose
            if (dealer_hand.IsBlackjack()) 
            {
                if (player_hand.IsBlackjack()) return 0.0; //dealer blackjack push
                else return -1; //dealer blackjack loss
            }
            if (player_hand.IsBlackjack()) return 1.5; //players hand is blackjack but dealer's is not.
            if (player_hand.GetScore() > dealer_hand.GetScore() || dealer_hand.IsBust()) return 1.0; //standard win
            else if (player_hand.GetScore() == dealer_hand.GetScore()) return 0.0; //standard push
            else return -1.0; //standard loss
        }

        public Hand GetPlayerHand()
        {
            return player_hand;
        }

        public Card GetDealerUpCard()
        {
            return dealer_hand.GetCard(0);
        }

        public Hand GetDealerHand()
        {
            return dealer_hand;
        }
    }
}
