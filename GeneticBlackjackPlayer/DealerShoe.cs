﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace BlackNeuron
{ 
    class DealerShoe
    {
        //holds the cards in the deck
        private List<Card> cards;
        //hold the number of decks used in the shoe
        private int number_of_decks;

        //Initializes and starts the first deck.
        public DealerShoe(int number_of_decks)
        {
            this.number_of_decks = number_of_decks;
            ResetDeck();
        }

        /* Shuffles the current deck. Note that the cards are removed one at a time so you wont have to call this outside */
        private void ShuffleCards()
        {
            //Basic shuffle algorithm, using our Random Number Generator though
            for (int i = 0; i < this.cards.Count; i++)
            {
                Card tmp = cards[i];
                ushort random_spot = (ushort)(RandomNumberGenerator.GetUint16() % this.cards.Count);
                cards[i] = cards[random_spot];
                cards[random_spot] = tmp;
            }
        }

        //Call this every time you want to reset the deck (put all the cards back into the shoe and reshuffle)
        public void ResetDeck()
        {
            cards = new List<Card>();

            for (int i = 0; i < number_of_decks; i++)
            {
                for (int x = 0; x < 52; x++)
                {
                    cards.Add(new Card(x));
                }
            }

            ShuffleCards();
        }

        //Dealers in casino blackjack will place a red card in the stack of cards near the end of the deck
        //This thwarts counters because whenever the red card is reached, the next round will require a shuffle
        public bool ShoeSuitableForNewGame()
        {
            //Dealers will usually place the read card about a quarter near the end of the deck
            return this.CardsLeft() > (.25*(number_of_decks*52)); 
        }

        //Returns the cards left in the shoe
        public int CardsLeft()
        {
            return this.cards.Count;
        }

        //Draws a card and removes it from the shoe
        public Card NextCard()
        {
            Card top_card = cards[0];
            cards.RemoveAt(0);
            return top_card;
        }
    }
}
