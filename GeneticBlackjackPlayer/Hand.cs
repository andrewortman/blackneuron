﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlackNeuron
{
    class Hand
    {
        public List<Card> cards;
        
        public void AddCard(Card c)
        {
            cards.Add(c);
        }

        public void ResetHand()
        {
            cards = new List<Card>();
        }

        public int GetSoftScore()
        {
            int score = 0;
            //this just adds all of the cards together by there value where ace is 1, and not 11
            for (int i = 0; i < cards.Count; i++)
            {
                score += cards[i].GetValue();
            }
            return score;
        }

        public int GetScore()
        {
            int score = GetSoftScore();
            for (int i = 0; i < cards.Count; i++)
            {
                if (cards[i].IsAceCard())
                {
                    if ((score + 10) <= 21) score += 10; //adds another 10 to the value until we hit 21.
                }
            }
            return score; //now the hard score.
        }

        public bool IsBust()
        {
            if (GetScore() > 21) return true;
            else return false;
        }

        public bool IsBlackjack()
        {
            return (GetNumberOfCards() == 2 && GetScore() == 21);
        }

        public Card GetCard(int index)
        {
            return cards[index];
        }

        public int GetNumberOfCards()
        {
            return cards.Count;
        }
    }
}
