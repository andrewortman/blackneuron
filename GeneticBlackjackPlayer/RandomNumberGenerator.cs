﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace BlackNeuron
{
    static class RandomNumberGenerator
    {
        private static RNGCryptoServiceProvider csp = new RNGCryptoServiceProvider();
        private static Random rand = new Random();

        //This is my random number generator. The regular Random class was giving me problems
        //because of a lack of entropy. This one can recalculate the entropy but is magnitudes slower
        public static ushort GetUint16()
        {
            byte[] random_number = new byte[2];
            csp.GetNonZeroBytes(random_number);

            return (ushort)(random_number[0] | (random_number[1] << 8));
        }

        public static double GetDouble()
        {
            return (double)GetUint16() / (double)UInt16.MaxValue;
        }
    }
}
