﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlackNeuron
{
    class GeneticSolver
    {
        //current population of the solver
        private List<BlackjackPlayerCandidate> current_population = new List<BlackjackPlayerCandidate>(Configuration.Genetic_Population);
        
        //the current generation of the solver
        private int current_generation = 0;

        //every time you run a simulation, this will be updated with the average advantage of the population
        private double average_advantage = 0.0; 

        //every time you run a simulation, this will be updated with the best performing candidate of the population
        //(but remember, this has to do with "luck")
        private BlackjackPlayerCandidate best_candidate;
        
        //Returns the generation number of the last simulated population (ie, run simulation and this will return "1" afterwords)
        public int GetGeneration() 
        {
            return current_generation;
        }

        public GeneticSolver()
        {
            //at first, generate random candidates
            for(int i = 0; i < Configuration.Genetic_Population; i++)
            {
                current_population.Add(new BlackjackPlayerCandidate(Configuration.Simulate_Rounds));
            }
        }

        // Returns the best candidate of the last simulation
        public BlackjackPlayerCandidate GetBestCandidate()
        {
            return best_candidate;
        }

        //Returns the average player advantage after a simulation (a number from -1->1 which represents the player's advantage as a percentage)
        public double GetAverageAdvantage()
        {
            return average_advantage;
        }

        //Run a single generation
        public void CalculateOnce()
        {
            //first go through the list and simulate.
            for(int i = 0; i < Configuration.Genetic_Population; i++)
            {
                current_population[i].RunSimulation();
            }

            List<BlackjackPlayerCandidate> new_population = new List<BlackjackPlayerCandidate>(Configuration.Genetic_Population);

            double population_score_sum = 0.0;
            //done, now we need to pick using a roullete wheel style selection
            //we have to get a sum though
            double population_advantage_sum = 0.0;
            foreach (BlackjackPlayerCandidate candidate in current_population)
            {
                population_advantage_sum += candidate.GetAdvantage();
                population_score_sum += candidate.GetScore();
            }

            average_advantage = population_advantage_sum / current_population.Count;

            //first we need to add the best of the best (the elites) to the new population
            double last_max = double.MaxValue;
            double current_search_max = 0;
            int current_search_index = 0;

            for(int i = 0; i < Configuration.Genetic_Elites; i++)
            {
                current_search_max = 0;
                current_search_index = 0;
                for(int x = 0; x < current_population.Count; x++)
                {
                    double score = current_population[x].GetScore();
                    if (score < last_max && score > current_search_max)
                    {
                        current_search_max = score;
                        current_search_index = x;
                    }
                }
                last_max = current_search_max;
                new_population.Add(current_population[current_search_index]);
            }
            //Best candidate is always at the front of the line. Might as well save some compute time and avoid another loop if we already found it
            best_candidate = new_population[0];

            //now start picking and choosing different candidates and breed them together
            for (int i = Configuration.Genetic_Elites; i < Configuration.Genetic_Population; i++)
            {
                //Pick two candidates based off their scores (higher score = bigger chances)
                double spot_a = population_score_sum * RandomNumberGenerator.GetDouble();

                List<double> DNA_A = new List<double>();
                List<double> DNA_B = new List<double>();
                double tmp_sum = 0.0;
                for (int x = 0; x < Configuration.Genetic_Population; x++)
                {
                    tmp_sum += current_population[x].GetScore();
                    if (tmp_sum >= spot_a)
                    {
                        DNA_A = current_population[x].GetDNA();
                        break;
                    }
                }
                do
                {
                    //do it again, except keep trying until we have two different strands of DNA
                    double spot_b = population_score_sum * RandomNumberGenerator.GetDouble();
                    tmp_sum = 0;
                    for (int x = 0; x < Configuration.Genetic_Population; x++)
                    {
                        tmp_sum += current_population[x].GetScore();
                        if (tmp_sum >= spot_b)
                        {
                            DNA_B = current_population[x].GetDNA();
                            break;
                        }
                    }
                } while (DNA_A == DNA_B);

                //now we have DNA_A and DNA_B
                //Cross their DNA at a random point to make new_DNA
                List<double> new_dna = new List<double>();
                int random_point = RandomNumberGenerator.GetUint16() % DNA_A.Count;
                new_dna.AddRange(DNA_A.GetRange(0, random_point));
                new_dna.AddRange(DNA_B.GetRange(random_point, DNA_B.Count - random_point));

                //now mutate randomly
                for (int x = 0; x < new_dna.Count; x++)
                {
                    if (RandomNumberGenerator.GetDouble() <= Configuration.Genetic_MutationRate)
                    {
                        //Mutate this weight by adding or subtracting by a number between 0 and the MutationFactor
                        new_dna[x] += (Configuration.Genetic_MutationFactor * (2 * RandomNumberGenerator.GetDouble() - 1));
                    }
                }

                //Create a new candidate objection and give it the new DNA. Add this candidate to the next population
                BlackjackPlayerCandidate new_candidate = new BlackjackPlayerCandidate(Configuration.Simulate_Rounds);
                new_candidate.SetDNA(new_dna);
                new_population.Add(new_candidate);
            }

            //We are done setting up the new_population, lets set it as the current one and increment the generation number
            current_population = new_population;
            current_generation++;
        }
    }
}
