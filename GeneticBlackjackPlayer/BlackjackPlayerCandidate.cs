﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace BlackNeuron
{
    class BlackjackPlayerCandidate
    {
        private BlackjackGame game;
        private int round_target = 0;
        private NeuralNet brain;
        public int wins, pushes, losses;

        public BlackjackPlayerCandidate(int number_of_rounds)
        {
            this.round_target = number_of_rounds;
            brain = new NeuralNet(9, Configuration.Neural_HiddenNeurons, 2);
            game = new BlackjackGame();
        }

        public List<double> GetDNA()
        {
            return brain.GetDNA();
        }

        public void SetDNA(List<double> dna)
        {
            brain.SetDNA(dna);
        }

        public int GetWins()
        {
            return wins;
        }

        public int GetLosses()
        {
            return losses;
        }

        public int GetPushes()
        {
            return pushes;
        }

        public double GetScore()
        {
            return (double)(5 * wins + .1*pushes);
        }
        public double GetAdvantage()
        {
            //Score = (wins-losses)/total;
            return (wins + pushes - losses) / (double)round_target;
        }

        public void RunSimulation()
        {
            wins = 0;
            losses = 0;
            pushes = 0;
            
            game.Reset(); //start a completely new game simulation

            for (int i = 0; i < round_target; i++)
            {
                game.NewRound();
                for (;;)
                {
                    //first input is dealer upcard
                    //second input is the first card in hand
                    //third input is the second card in hand
                    //...
                    //nineth input is the eigth card in hand.

                    //if no card in hand, input is a -1

                    List<double> inputs = new List<double>(9);
                    inputs.Add(game.GetDealerUpCard().GetValue());
                    Hand player_hand = game.GetPlayerHand();
                    List<double> player_hand_values = new List<double>(player_hand.cards.Count);
                    
                    for(int x = 0; x < player_hand.cards.Count; x++)
                    {
                        player_hand_values.Add(player_hand.cards[x].GetValue());
                    }
                    player_hand_values.Sort();

                    int cnt = player_hand_values.Count;
                    if (cnt > 8) cnt = 8;
                    for (int x = 0; x < cnt; x++)
                    {
                        inputs.Add(player_hand_values[x]);
                    }
                    for (int x = cnt; x < 8; x++)
                    {
                        inputs.Add(-1.0);
                    }

                    List<double> decisions = brain.Fire(inputs);
                    double max_value = decisions[0];
                    double max_index = 0;
                    for (int x = 1; x < decisions.Count; x++)
                    {
                        if (decisions[x] > max_value)
                        {
                            max_value = decisions[x];
                            max_index = x;
                        }
                    }

                    if (max_index == 0) //first output is 'HIT'
                    {
                        game.PlayerHit();
                        if(game.GetPlayerHand().IsBust()) break; //if we busted we have to bail out always
                    }
                    else if (max_index == 1) //second output is 'STAND'
                    {
                        game.DealerPlay(); //let the dealer play
                        break;
                    }
                }

                double results = game.GetResults();
                if (results < 0)
                {
                    losses++;
                }
                else if (results == 0)
                {
                    pushes++;
                }
                else if (results > 0)
                {
                    wins++;
                }
            }
        }
    }
}
