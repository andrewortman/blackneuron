﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace BlackNeuron
{
    class Card
    {
        int index  = 0;
        
        /* card_index is a number between 0 and 51 representing the index of the card in the deck */
        public Card(int card_index)
        {
            Debug.Assert(card_index >= 0 && card_index <= 51, "Cannot create card - card index invalid range (" + card_index + ")");
            this.index = card_index;
        }

        //Returns the card's "number" (1->13 where 1 is ace, 2 is "2", 11 is "jack", 13 is "king", etc)
        private int GetCardNumber()
        {
            return (index/4)+1;
        }

        private int GetCardFaceIndex()
        {
            return (index%4);
        }

        public bool IsFaceCard()
        {
            int number = GetCardNumber();
            return (number == 11 || number == 12 || number == 13);
        }

        public bool IsAceCard()
        {
            return GetCardNumber() == 1;
        }

        public int GetValue()
        {
            if (GetCardNumber() <= 10) return GetCardNumber();
            else return 10; //Face cards are worth ten, everything else is worth their number; Aces return "1" but can be "11" 
        }

        //This is useful when debugging. Returns a human readable string of the card
        public override string ToString()
        {
            string[] values = { "ACE", "TWO", "THREE", 
                                "FOUR", "FIVE", "SIX",
                                "SEVEN", "EIGHT", "NINE",
                                "TEN", "JACK", "QUEEN", "KING" };

            string[] faces = { "HEARTS", "DIAMONDS", "CLUBS", "SPADES" };

            return values[GetCardNumber() - 1] + " of " + faces[GetCardFaceIndex()];
        }
    }
}
