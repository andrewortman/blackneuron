﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace BlackNeuron
{
    class Neuron
    {
        //Stores the input weights for each neuron
        public List<double> weights = new List<double>();

        //Create a random neuron
        public Neuron(int number_of_inputs)
        {
            //Generate a set of random weights between -2 and 2 (this range doesnt really matter though, just a hunch)
            for (int i = 0; i < number_of_inputs; i++)
            {
                weights.Add((2 * RandomNumberGenerator.GetDouble() - 1)); //-1->1
            }
        }

        //Create a new, nonrandom neuron
        public Neuron(List<double> weights)
        {
            this.weights = weights;
        }

        //Replaces the weights
        public void SetWeights(List<double> weights)
        {
            Debug.Assert(this.weights.Count == weights.Count, "parameter weight and weight count mismatch");
            this.weights.Clear();
            this.weights.AddRange(weights);
        }

        //Returns a list of weights for combination at the network level and represented as the DNA of the system
        public List<double> GetWeights()
        {
            return this.weights;
        }

        //Individual neuron fire.
        public double Fire(List<double> inputs)
        {
            Debug.Assert(inputs.Count == weights.Count, "Input / Weight count mismatch");
            double sum = 0.0;
            for (int i = 0; i < weights.Count; i++)
            {
                sum += weights[i] * inputs[i];
            }

            return ActivationFunction(sum);
        }

        //Regular Sigmoid Activation Function
        public double ActivationFunction(double input)
        {
            return (1.0 / (1.0 + Math.Exp(-input)));
        }
    }

    //Represents a simple three layer MLP network
    class NeuralNet
    {
        List<Neuron> input_layer, hidden_layer, output_layer;
        int input_count, hidden_count, output_count;

        //Initializes a neural net with dynamic inputs and output nodes
        public NeuralNet(int number_input, int number_hidden, int number_output)
        {
            input_count = number_input;
            output_count = number_output;
            hidden_count = number_hidden;
            input_layer = new List<Neuron>(input_count);
            hidden_layer = new List<Neuron>(hidden_count);
            output_layer = new List<Neuron>(output_count);

            //initialize the layers
            for (int i = 0; i < input_count; i++)
            {
                input_layer.Add(new Neuron(1));
            }
            for (int i = 0; i < hidden_count; i++)
            {
                hidden_layer.Add(new Neuron(input_count));
            }
            for (int i = 0; i < output_count; i++)
            {
                output_layer.Add(new Neuron(hidden_count));
            }
        }
        public NeuralNet(int number_input, int number_hidden, int number_output, List<double> DNA)
        {
            input_count = number_input;
            output_count = number_output;
            hidden_count = number_hidden;

            input_layer = new List<Neuron>(input_count);
            hidden_layer = new List<Neuron>(hidden_count);
            output_layer = new List<Neuron>(output_count);

            SetDNA(DNA);
        }

        //Basically sets the weights of each neuron.
        //The DNA is a string of doubles (each represents a weight) that is ordered
        //first by the weight index and then by the neuron index and finally by the layer
        public void SetDNA(List<double> DNA)
        {
            input_layer.Clear();
            hidden_layer.Clear();
            output_layer.Clear();
            //initialize the layers

            for (int i = 0; i < input_count; i++)
            {
                input_layer.Add(new Neuron(DNA.GetRange(i, 1)));
            }

            for (int i = 0; i < hidden_count; i++)
            {
                hidden_layer.Add(new Neuron(DNA.GetRange(input_count + (input_count * i), input_count)));
            }

            for (int i = 0; i < output_count; i++)
            {
                output_layer.Add(new Neuron(DNA.GetRange(input_count + (input_count * hidden_count) + (hidden_count * i), hidden_count)));
            }
        }

        //Basically gets the weights of each neuron.
        //The DNA is a string of doubles (each represents a weight) that is ordered
        //first by the weight index and then by the neuron index and finally by the layer
        public List<double> GetDNA()
        {

            List<double> output = new List<double>(input_count + (input_count * hidden_count) + (output_count * hidden_count));
            for (int i = 0; i < input_count; i++)
            {
                output.AddRange(input_layer[i].GetWeights());
            }
            for (int i = 0; i < hidden_count; i++)
            {
                output.AddRange(hidden_layer[i].GetWeights());
            }
            for (int i = 0; i < output_count; i++)
            {
                output.AddRange(output_layer[i].GetWeights());
            }

            return output;
        }

        //Fires a set of inputs and returns the outputs.
        public List<double> Fire(List<double> inputs)
        {
            Debug.Assert(inputs.Count == input_layer.Count, "input layer count and input count mismatch");
            List<double> input_outputs, hidden_outputs, output_outputs;

            input_outputs = new List<double>(input_count);
            hidden_outputs = new List<double>(hidden_count);
            output_outputs = new List<double>(output_count);

            for (int i = 0; i < input_count; i++)
            {
                List<double> input = new List<double>();
                input.Add(inputs[i]);
                input_outputs.Add(input_layer[i].Fire(input));
            }


            for (int i = 0; i < hidden_count; i++)
            {
                hidden_outputs.Add(hidden_layer[i].Fire(input_outputs));
            }

            for (int i = 0; i < output_count; i++)
            {
                output_outputs.Add(output_layer[i].Fire(hidden_outputs));
            }
            return output_outputs;
        }
    }
}
