﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;

namespace BlackNeuron
{
    class Program
    {
        static void Main(string[] args)
        {
            GeneticSolver solver = new GeneticSolver();
            while (true)
            {
                Console.Write("Solving Generation " + (solver.GetGeneration() + 1) + ".. ");
                solver.CalculateOnce();
                Console.WriteLine("Solved! \nBest candidate advantage: " + solver.GetBestCandidate().GetAdvantage() + "\nPopulation Average Advantage: " + solver.GetAverageAdvantage());
                Console.WriteLine();
            }
        }

    }
}
